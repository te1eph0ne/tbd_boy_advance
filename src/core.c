#include <stdio.h>
#include <limits.h>
#include <assert.h>
#include "core.h"
#include "settings.h"

// Data Formats
// ------------
// 8bit  - Byte
// 16bit - Halfword
// 32bit - Word
typedef unsigned char BYTE;
typedef unsigned short HALFWORD;
typedef unsigned int WORD;

unsigned int getCoreDataSizes() {
#if DEBUG == 2
  printf("BYTE: %d\n", BSIZE(BYTE));
  printf("HALFWORD: %d\n", BSIZE(HALFWORD));
  printf("WORD: %d\n", BSIZE(WORD));
#endif
  if(BSIZE(BYTE)     ==  8  &&
     BSIZE(HALFWORD) == 16  &&
     BSIZE(WORD)     == 32 ) {
    return 1; // Success
  } else {
    return 0; // Failure
  }
}

int coreTests() {
  assert(getCoreDataSizes());
}
